<h1>Раздел LINUX.</h1>

<h2>Part 1. Установка ОС</h2>

1. Скачиваем дистрибутив ISO с официального сайта http://ubuntu.com
2. Подключаем ISO к новой созданной VM
3. Включаем опцию UEFI в настройках VM
4. В bootloader выбираем Try or Install Ubuntu
5. Устанавливаем дистрибутив с настройками по-умолчанию (серверная сборка идет уже без графического интерфейса)
6. Проверка установленной версии сервера
<br><img src="images/img1_01.png" alt="cat /etc/issue">

<h2>Part 2. Создание пользователя</h2>

1. Создадим пользователя <u>s21user</u> с паролем <u>school21</u> и добавляем в группу <u>adm</u>. Вводим команду 
<br><b>sudo adduser -p school21 -s /bin/bash s21user -G adm</b>
<br><img src=images/img2_01.png>
2. Вывод команды <br><b>cat /etc/passwd</b> <br>В последней строчке наш пользователь.
<br><img src=images/img2_02.png>

<h2>Part 3. Настройка сети ОС</h2>
1. Изменение имени машины
<br><img src=images/img3_01.png>
<br><img src=images/img3_02.png>
<br><img src=images/img3_03.png>
2. Изменение временной зоны
<br><img src=images/img3_04.png>
3. Названия сетевых интерфейсов
<br><img src=images/img3_05.png>
<br>Интерфейс lo - виртуальный интерфейс с именем localhost по адресу 127.0.0.1 для обращения по сети к самой машине, на которой мы работаем.
4. Получить ip-адрес устройства
<br><img src=images/img3_06.png>
DHCP - Dynamic Host Configuration Protocol  - протокол динамической настройки узла.
5. Адреса шлюзов
<br><img src=images/img3_07.png>
6. Задаем статические адреса
<br>Настраиваем конфигурацию netplan следующим образом
<br><img src=images/img3_08.png>
<br><img src=images/img3_09.png>
<br>Применяем netplan apply, проверяем настройки сети. Адрес enp0s3 изменился с DHCP 10.0.2.15 на STATIC 10.0.2.16
<br><img src=images/img3_10.png>

7. Перезагрузка машины, проверка настроек
<br><b>	reboot now</b>
<br><img src=images/img3_11.png>

8. Пинг хостов 1.1.1.1 и ya.ru
<br><img src=images/img3_12.png>

<h2>Part 4. Обновление ОС</h2>
sudo apt update
<br>sudo apt upgrade
<br><img src=images/img4_01.png>

<h2>Part 5. Использование команды sudo</h2>
<ol>
<li>Проверка текущего наименлования хоста
<li>Попытка изменить хостнейм от имени s21user
<li>При получении сообщения об ограниченных правах для пользователя добавляем его в группу sudo
<li>Снова пытаемся сменить название хоста
<li>Получилось
</ol>
<img src=images/img5_01.png>

<h2>Part 6. Установка и настройка службы времени</h2>
<img src=images/img6_01.png>
<br>apt install systemd-timesyncd
<br><img src=images/img6_02.png>

<h2>Part 7. Установка и использование текстовых редакторов</h2>
<b>Редактирование</b>
<br>vi test_vi.txt
<br>Клавиша INSERT -> willumye -> Esc -> :wq
<br><img src=images/img7_01.png>
<br>
<br>nano test_nano.txt -> willumye -> Ctrl+O -> Ctrl+X
<br><img src=images/img7_02.png>
<br>
<br>touch test_mc.txt -> mc -> F4 -> willumye -> F2 -> Esc
<br><img src=images/img7_03.png>
<br><b>Выход без изменений</b>
<br>vi test_vi.txt -> 21 School 21 -> :q!
<br><img src=images/img7_04.png>
<br>
<br>nano test_nano.txt -> 21 School 21 -> Ctrl+X -> N (No)
<br><img src=images/img7_05.png>
<br>
<br>mc -> F4 -> 21 School 21 -> Esc -> No
<br><img src=images/img7_06.png>
<br>
<br<b>Результат</b>
<br><img src=images/img7_07.png>
<br<b>Замена слов</b>
<br>vi test_vi.txt -> :s/willumye/21School -> :wq
<br><img src=images/img7_08.png>
<br>nano test_nano.txt -> Ctrl+\ -> willumye -> 21School -> Yes -> Ctrl+O -> Ctrl+X
<br><img src=images/img7_09.png>
<br>mc -> F4 (Edit)-> F4 (Replace) -> willumye / 21School -> F2 (Save) -> Esc
<br><img src=images/img7_10.png>
<br><img src=images/img7_11.png>
<br<b>Результат</b>
<br><img src=images/img7_12.png>

<h2>Part 8. Установка и базовая настройка сервиса SSHD</h2>
1. Установить службу SSHd.
<ul>
<li>  sudo apt update
<li>  sudo apt install ssh
<li>  sudo apt install openssh-server
</ul>

2. Автостарт при запуске системы
    sudo systemctl enable ssh

3. Перенастройка порта
    nano /etc/ssh/sshd_config
    Добавляем Port 2022
<br><img src=images/img8_00.png>

4. Проверка запуска службы sshd, перезапуск, просмотр процесса
<br><img src=images/img8_01.png>
<br> `ps` - команда, которая выводит статистику и информацию о состоянии процессов в системе, в том числе ИД процесса или нити, объем выполняемого ввода-вывода и используемый объем ресурсов процессора и памяти. \
Список ключей: \
-A, -e, (a) - выбрать все процессы; \
-a - выбрать все процессы, кроме фоновых; \
-d, (g) - выбрать все процессы, даже фоновые, кроме процессов сессий; \
-N - выбрать все процессы кроме указанных; \
-С - выбирать процессы по имени команды; \
-G - выбрать процессы по ID группы; \
-p, (p) - выбрать процессы PID; \
--ppid - выбрать процессы по PID родительского процесса; \
-s - выбрать процессы по ID сессии; \
-t, (t) - выбрать процессы по tty; \
-u, (U) - выбрать процессы пользователя.

5. Гасим систему `sudo shutdown now`
6. Пробрасываем порт 2022 на VirtualBox, запускаем виртуальную машину
7. Запускаем терминал, вводим ssh willumye@127.0.0.1 -p 2022
<br>Подключаемся к виртуальной машине.

8. netstat -tan
<br><img src=images/img8_02.png> 
<br>`netstat` - команда для вывода списка открытых портов и соответствующих им адресов, таблиц маршрутизации и скрытых соединений. 
Список ключей: 
<br>-t - Отображение текущего подключения в состоянии переноса нагрузки с процессора на сетевой адаптер при передаче данных ( "offload" ); 
<br>-a - Отображение всех подключений и ожидающих портов; 
<br>-n - Отображение адресов и номеров портов в числовом формате.
<br>`Proto` - Протокол, используемый сокетом.
<br>`Recv-Q` - Счётчик байт не скопированных программой пользователя из этого сокета.
<br>`Send-Q` - Счётчик байтов, не подтверждённых удалённым узлом.
<br>`Local Address` - Адрес и номер порта локального конца сокета. Если не указана опция --numeric (-n), адрес сокета преобразуется в каноническое имя узла (FQDN), и номер порта преобразуется в соответствующее имя службы.
<br>`Foreign Address` - Адрес и номер порта удалённого конца сокета. Аналогично "Local Address"
<br>`State` - Состояние сокета.
<br>`0.0.0.0` -  это немаршрутизируемый адрес IPv4, который используется в качестве адреса по умолчанию или адреса-заполнителя.
 
<h2>Part 9. Установка и использование утилит top, htop</h2>
<ol>
<li>Вызов команды top. <br>
uptime - 53:00 <br>
количество авторизованных пользователей - 4 <br>
общая загрузка системы - 0.00, 0.00, 0.00<br>
общее количество процессов - 107 <br>
загрузка cpu - 0.0 us, 0.7 sy, 0.0 ni, 99.3 id, 0.0 wa, 0.0 hi, 0.0 si, 0.0 st <br>
загрузка памяти - 1951.5 total, 1454.3 free, 189.2 used, 307.9 buff/cache <br>
pid процесса занимающего больше всего памяти - 1<br>
pid процесса, занимающего больше всего процессорного времени - 1708
<br><img src=images/img9_01.png> 

<li>Вызов команды htop. <br>
<ul>
<li>htop / Сортировка по PERCENT_CPU по умолчанию
<br><img src=images/img9_02.png>
<li>Сортировка по PID
<br><img src=images/img9_08.png>
<li>Сортировка по PERCENT_MEM
<br><img src=images/img9_03.png>
<li>Сортировка по TIME
<br><img src=images/img9_04.png>
<li>Фильтр по sshd
<br><img src=images/img9_05.png>
<li>Поиск по syslog
<br><img src=images/img9_06.png>
<li>Добавлен вывод hostname, clock и uptime
<br><img src=images/img9_07.png>
</ul>
</ol>

<h2>Part 10. Использование утилиты fdisk</h2>
<ol>
<li>Запускаем команду fdisk -l для просмотра данных о жестком диске
<br><img src=images/img10_01.png>
<ul>
<li>Disk /dev/sda: 10 GiB, 10737418240 bytes, 20971520 sectors - название диска, размер, кол-во секторов
<li>Disk model: VBOX HARDDISK - модель диска
<li>Логические диски:
<br>Device       Start      End  Sectors  Size Type
<br>/dev/sda1     2048  1103871  1101824  538M EFI System
<br>/dev/sda2  1103872  4773887  3670016  1.8G Linux filesystem
<br>/dev/sda3  4773888 20969471 16195584  7.7G Linux filesystem
</ul>

<li>Получение информации о размере swap-файла на жёстком диске
<br><img src=images/img10_02.png>
</ol>

<h2>Part 11. Использование утилиты df</h2>
<ol>
<li>df / получение информации о дисковом пространстве
<br><img src=images/img11_01.png><br>
Размер раздела - 7865580; <br>
Размер занятого пространства - 5382588; <br>
Размер свободного пространства - 2061924; <br>
Процент использования - 73;<br>
Единица измерения - 1K-blocks (килобайт).<br>

<li>Запустить команду df -Th / получение информации о дисковом пространстве с типом файловой системы и размерами в читаемом виде
<br><img src=images/img11_02.png><br>
Размер раздела - 7.6G; <br>
Размер занятого пространства - 5.2G; <br>
Размер свободного пространства - 2.0G; <br>
Процент использования - 73;<br>
Тип файловой системы - ext4<br>
</ol>

<h2>Part 12. Использование утилиты du</h2>
<ol>
<li>du / получение информации о размере всех файлов в определённой папке
<br><img src=images/img12_01.png>
<li>Получение информации о размере папок /home, /var, /var/log в байтах
<br><img src=images/img12_02.png>
<li>Получение информации о размере папок /home, /var, /var/log в человекочитаемом виде.
<br><img src=images/img12_03.png>
<li>Получение информации о размере всего содержимого в /var/log в человекочитаемом виде (не общее, а каждого вложенного элемента, используя *)
<br><img src=images/img12_04.png>
</ol>

<h2>Part 13. Установка и использование утилиты ncdu</h2>
<ol>
<li>apt install ncdu
<li>ncdu /home
<br><img src=images/img13_01.png>
<li>ncdu /var
<br><img src=images/img13_02.png>
<li>ncdu /var/log
<br><img src=images/img13_03.png>
</ol>

<h2>Part 14. Работа с системными журналами</h2>
<ol>
<li>cat /var/log/dmesg
<br><img src=images/img14_01.png>
<li>cat /var/log/syslog
<br><img src=images/img14_02.png>
<li>cat /var/log/auth.log
<br><img src=images/img14_03.png>
<br>Feb 14 09:17:01 ubuntu2204-server-ws1 CRON[3274]: pam_unix(cron:session): session opened for user root(uid=0) by (uid=0)
<br>Время: 14 февраля 09:17:01
<br>Имя: root
<br>Метод: pam_unix
<li>Перезапуск службы sshd / systemctl restart sshd -> tail /var/log/syslog [хвост лога]
<br><img src=images/img14_04.png>
</ol>

<h2>Part 15. Использование планировщика заданий CRON</h2>
<ol>
<li>Формат cron: минута час день месяц день_недели /путь/к/исполняемому/файлу
<br>Посмотреть задачи cron для суперпользователя, для этого можно воспользоваться опцией -l:
<br>`crontab -l`
<br><img src=images/img15_01.png>
<li>crontab -e
<br>Добавляем исполнение uptime каждые 2 минуты (для этого используется косая черта, иначе задача будет выполняться каждую 2-ю минуту часа)
<br><img src=images/img15_02.png>
<br>Проверка crontab -l
<br><img src=images/img15_03.png>
<li>Данные логов о выполнении cron
<br><img src=images/img15_04.png>
<li>Очистка заданий планировщика / crontab -r -> crontab -l
<br><img src=images/img15_05.png>
</ol>





